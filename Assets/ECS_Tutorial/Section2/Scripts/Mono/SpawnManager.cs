﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Section2
{
    public class SpawnManager : MonoBehaviour
    {
        public GameObject prefab;
        public int amount = 2000;

        void Start()
        {
            for (int i = 0; i < amount; i++)
            {
                Vector3 pos = Vector3.one;
                pos.y = 0;
                pos.x = Random.Range(-50, 50);
                pos.z = Random.Range(-50, 50);
                GameObject.Instantiate(prefab, pos, Quaternion.identity);
            }
        }
    }
}