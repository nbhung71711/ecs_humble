﻿//#define S2

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Entities;
using Unity.Jobs;

namespace Section2
{

    public class S2_MoveSystem : JobComponentSystem
    {
        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
#if S2
            var jobHandle = Entities.WithName("S2_MoveSystem")
                .ForEach((ref Translation position, ref Rotation rotation) =>
                {
                    //position.Value += 0.1f * math.forward(rotation.Value);
                    //if (position.Value.z > 50)
                    //    position.Value.z = -50;

                    position.Value.y += 0.1f;
                    if (position.Value.z > 50)
                        position.Value.z = 0;
                })
                .Schedule(inputDeps);
            return jobHandle;
#endif
            return inputDeps;
        }
    }

}