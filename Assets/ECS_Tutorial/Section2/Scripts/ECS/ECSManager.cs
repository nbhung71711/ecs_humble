﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace Section2
{
    public class ECSManager : MonoBehaviour
    {
        public EntityManager entityManager;
        public GameObject sheepPrefab;
        public int numSheep = 15000;

        private void Start()
        {
            entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            var settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, null);
            var prefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(sheepPrefab, settings);

            for (int i = 0; i < numSheep; i++)
            {
                var sheepInstance = entityManager.Instantiate(prefab);
                //var sheepPos = transform.TransformPoint(new float3(UnityEngine.Random.Range(-50, 50), 0, UnityEngine.Random.Range(-50, 50)));
                var sheepPos = transform.TransformPoint(new float3(UnityEngine.Random.Range(-50, 50), UnityEngine.Random.Range(-50, 50), UnityEngine.Random.Range(-50, 50)));
                entityManager.SetComponentData(sheepInstance, new Translation { Value = sheepPos });
                entityManager.SetComponentData(sheepInstance, new Rotation { Value = new Quaternion(0, 0, 0, 0) });
            }
        }
    }
}