﻿//#define S4

using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Transforms;

namespace Section4
{
    public class S4_MoveSystem : JobComponentSystem
    {
        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
#if S4
            float deltaTime = Time.DeltaTime;
            float speed = 20f;
            var targetLocation = new float3(0, 0, 0);
            var jobHandle = Entities
                .ForEach((ref Translation position, ref Rotation rotation, ref S4_SphereData data) =>
                {
                    position.Value.y += 0.01f;
                    if (position.Value.z > 50)
                        position.Value.z = 0;

                    float3 pivot = targetLocation;
                    float rotSpeed = deltaTime * speed * 1 / math.distance(position.Value, pivot);
                    position.Value = math.mul(quaternion.AxisAngle(new float3(0, 1, 0), rotSpeed), position.Value - pivot) + pivot;
                })
                .Schedule(inputDeps);
            return jobHandle;
#endif
            return inputDeps;
        }
    }
}