﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Section4
{
    public class S4_Manager : MonoBehaviour
    {
        public EntityManager entityManager;
        public GameObject spherePurePrefab;
        public int spawnNumber = 5000;

        private void Start()
        {
            entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            var settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, null);
            var ecsPrefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(spherePurePrefab, settings);

            var sphereMeshRenderer = spherePurePrefab.GetComponent<MeshRenderer>();
            var sphereMeshFilter = spherePurePrefab.GetComponent<MeshFilter>();

            for (int i = 0; i < spawnNumber; i++)
            {
                var sphereInstance = entityManager.Instantiate(ecsPrefab);
                entityManager.AddComponent(sphereInstance, typeof(Translation));
                entityManager.AddComponent(sphereInstance, typeof(Rotation));
                entityManager.AddComponent(sphereInstance, typeof(S4_SphereData));

                float x = Mathf.Sin(i) * Random.Range(25, 50);
                float y = Random.Range(-5, 5);
                float z = Mathf.Cos(i) * Random.Range(25, 50);
                var position = transform.TransformPoint(new float3(x, y, z));
                entityManager.SetComponentData(sphereInstance,
                    new Translation { Value = position });

                var q = Quaternion.Euler(new Vector3(0, 0, 0));
                entityManager.SetComponentData(sphereInstance,
                    new Rotation { Value = new quaternion(q.x, q.y, q.z, q.w) });

                var scale = new float3(Random.Range(1, 3), Random.Range(1, 3), Random.Range(1, 3));
                entityManager.AddComponent(sphereInstance, typeof(NonUniformScale));
                entityManager.SetComponentData(sphereInstance,
                    new NonUniformScale { Value = scale });
            }
        }
    }
}