﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using UnityEngine.UI;

namespace Section3
{
    public class S3_UI : MonoBehaviour
    {
        [SerializeField] private Button mBtn;

        void Start()
        {
            mBtn.onClick.AddListener(BtnPressed);
        }

        private void BtnPressed()
        {
        }
    }
}