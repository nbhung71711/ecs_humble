﻿//#define S3

using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;

namespace Section3
{

    public class S3_MoveSheepsSystem : JobComponentSystem
    {
        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
#if S3
            var jobHandle = Entities
                .ForEach((ref Translation position, ref Rotation rotation, ref SheepData sheep) =>
                {
                    position.Value.y += 0.01f;
                    if (position.Value.z > 50)
                        position.Value.z = 0;
                })
                .Schedule(inputDeps);
            return jobHandle;
#endif
            return inputDeps;
        }
    }

    public class S3_MoveCubeSystem : JobComponentSystem
    {
        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
#if S3
            var jobHandle = Entities
                .ForEach((ref Translation position, ref Rotation rotation, ref CubeData cube) =>
                {
                    position.Value.y -= 0.01f;
                    if (position.Value.z > 50)
                        position.Value.z = 0;
                })
                .Schedule(inputDeps);
            return jobHandle;
#endif
            return inputDeps;
        }
    }

    public class S3_CapsuleSystem : JobComponentSystem
    {
        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            return inputDeps;
        }

        protected override void OnCreate()
        {
            base.OnCreate();

#if S3
            for (int i = 0; i < 100; i++)
                CreateInstance();
#endif
        }

        private void CreateInstance()
        {
            var instance = EntityManager.CreateEntity(
                ComponentType.ReadOnly<LocalToWorld>(),
                ComponentType.ReadOnly<Translation>(),
                ComponentType.ReadOnly<Rotation>(),
                ComponentType.ReadOnly<NonUniformScale>(),
                ComponentType.ReadOnly<RenderMesh>());

            var pos = GetRandomFloat3();
            float scale = UnityEngine.Random.Range(10, 30);
            //EntityManager.SetComponentData(instance,
            //    new LocalToWorld()
            //    {
            //        Value = new float4x4(quaternion.identity, pos)
            //    });
            EntityManager.SetComponentData(instance,
                new Translation()
                {
                    Value = pos
                });
            EntityManager.SetComponentData(instance,
                new Rotation
                {
                    Value = quaternion.identity
                });
            EntityManager.SetComponentData(instance,
                new NonUniformScale()
                {
                    Value = new float3(scale, scale, scale)
                });
            var asset = Resources.Load<ResoucesHolder>("Capsule");
            EntityManager.SetSharedComponentData(instance, new RenderMesh()
            {
                mesh = asset.mesh,
                material = asset.mat,
            });
        }

        private float3 GetRandomFloat3()
        {
            return new float3(UnityEngine.Random.Range(-10, 10), 0, UnityEngine.Random.Range(-10, 10));
        }
    }
}