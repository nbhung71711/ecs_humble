﻿//#define S3

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Entities;

namespace Section3
{
    public class S3_ECSInterface : MonoBehaviour
    {
        [SerializeField] private GameObject sheepPurePrefab;
        [SerializeField] private GameObject tankPurePrefab;
        [SerializeField] private GameObject sheepEntityPrefab;
        [SerializeField] private GameObject tankEntityPrefab;

        private World mWorld;

        private void Start()
        {
            mWorld = World.DefaultGameObjectInjectionWorld;

            Debug.Log("All Entities: " + mWorld.GetExistingSystem<S3_MoveSheepsSystem>().EntityManager.GetAllEntities().Length);

            EntityManager entityManager = mWorld.GetExistingSystem<S3_MoveSheepsSystem>().EntityManager;
            EntityQuery entityQuerySheep = entityManager.CreateEntityQuery(ComponentType.ReadOnly<SheepData>());
            EntityQuery entityQueryTank = entityManager.CreateEntityQuery(ComponentType.ReadOnly<TankData>());

            Debug.Log("All Sheep Entities: " + entityQuerySheep.CalculateEntityCount());
            Debug.Log("All Tank Entities: " + entityQueryTank.CalculateEntityCount());
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
                var settings = GameObjectConversionSettings.FromWorld(mWorld, null);
                var convertedSheepPrefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(sheepPurePrefab, settings);
                var convertedTankPrefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(tankPurePrefab, settings);

                var sheepInstance = entityManager.Instantiate(convertedSheepPrefab);
                entityManager.SetComponentData(sheepInstance, new Translation { Value = GetRandomFloat3() });
                entityManager.SetComponentData(sheepInstance, new Rotation { Value = Quaternion.identity });
                entityManager.AddComponent<SheepData>(sheepInstance);

                var tankInstance = entityManager.Instantiate(convertedTankPrefab);
                entityManager.SetComponentData(tankInstance, new Translation { Value = GetRandomFloat3() });
                entityManager.AddComponent(tankInstance, typeof(TankData));
            }
            else if (Input.GetKeyDown(KeyCode.P))
            {
                Instantiate(sheepEntityPrefab, GetRandomFloat3(), Quaternion.identity, null);
                Instantiate(tankEntityPrefab, GetRandomFloat3(), Quaternion.identity, null);
            }
        }

        private float3 GetRandomFloat3()
        {
            return new float3(UnityEngine.Random.Range(-10, 10), 0, UnityEngine.Random.Range(-10, 10));
        }
    }
}