﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Section3
{
    public enum GameState
    {
        Home,
        GamePlay,
        Pause,
        GameOver,
    }

    public class GameManager : MonoBehaviour
    {
        public static GameManager instance;

        [Header("Home Panel")]
        public GameObject homePanel;

        [Header("Gameplay Panel")]
        public GameObject gameplayPanel;
        public int score;
        public DotsSpawner dotsSpawner;
        public TextMeshProUGUI txtScore;
        public PlayerMovement player;
        public TextMeshProUGUI txtCountdown;
        public float duration = 15;

        [Header("Pause Panel")]
        public GameObject pausePanel;

        [Header("GameOver Panel")]
        public GameObject gameOver;
        public TextMeshProUGUI txtHighscore;

        private GameState mCurrentState;
        private float mCoutdown;

        private void Awake()
        {
            if (instance == null)
                instance = this;
            else if (instance != this)
                Destroy(gameObject);
        }

        private void Start()
        {
            SetState(GameState.Home);
            StartCoroutine(IEUpdate());
        }

        private void Update()
        {
            if (mCurrentState == GameState.GamePlay)
            {
                if (mCoutdown > 0)
                    mCoutdown -= Time.deltaTime;
                else
                    SetState(GameState.GameOver);
            }
        }

        private IEnumerator IEUpdate()
        {
            WaitForSeconds interval = new WaitForSeconds(0.5f);
            while (true)
            {
                if (mCurrentState == GameState.GamePlay)
                {
                    txtCountdown.text = "Time left:" + Mathf.RoundToInt(mCoutdown);
                }
                yield return interval;
            }
        }

        private void SetState(GameState pCurrentState)
        {
            mCurrentState = pCurrentState;

            if (mCurrentState == GameState.GameOver)
            {
                if (GameData.HighScore < score)
                    GameData.HighScore = score;
                txtHighscore.text = "High Score: " + GameData.HighScore;
            }

            homePanel.SetActive(mCurrentState == GameState.Home);
            gameplayPanel.SetActive(mCurrentState == GameState.GamePlay);
            pausePanel.SetActive(mCurrentState == GameState.Pause);
            gameOver.SetActive(mCurrentState == GameState.GameOver);

            if (mCurrentState == GameState.Pause)
                Time.timeScale = 0;
            else
                Time.timeScale = 1;

            if (mCurrentState == GameState.GamePlay)
            {
                player.active = true;
                dotsSpawner.active = true;
            }
            else
            {
                player.active = false;
                dotsSpawner.active = false;
            }
        }

        private void StartGame()
        {
            //TODO: Create Player and Enemies
            score = 0;
            mCoutdown = duration;
            txtScore.text = "Score: " + score;
            dotsSpawner.StartGame();
        }

        public void BtnGoHome_Pressed()
        {
            SetState(GameState.Home);
        }

        public void BtnPlay_Pressed()
        {
            SetState(GameState.GamePlay);
            StartGame();
        }

        public void BtnPause_Pressed()
        {
            SetState(GameState.Pause);
        }

        public void BtnContinue_Pressed()
        {
            SetState(GameState.GamePlay);
        }

        public void AddScore(int pScore)
        {
            score += pScore;
            txtScore.text = "Score: " + score;
        }
    }

    public static class GameData
    {
        public static int HighScore
        {
            get { return PlayerPrefs.GetInt("HighScore"); }
            set { PlayerPrefs.SetInt("HighScore", value); }
        }
    }
}