﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Section3
{
    //Preparation:
    //A package of a completed UI and with 5 state Home, GamePlay, Pause, Win, Lose
    //Make name game project and import full UI system which is prepared

    //Prologue: 
    // I. Introduction
    // 1: Introduce game idle, gameplay
    // 2: Introduce new components (SpriteRenderer, Collision) and Prefab
    // 3: Introduce collision system

    //II. Create player and make it moveable
    // 2: create a gameobject, name it player. Drag drop it into GameManager gameobject
    // 3: In project window, create a sprite, name it circle and put it into SpriteRenderer component of player gameobject.
    // 4: create a monobehaviour scirpt, name it PlayerMovement then drag drop to player.
    // 4.1: Declare a integer field for player movement speed
    // 4.2: Let make player move toward x and y
    // 6: Test movement script
    // 7: Make camera follow player
    // 7.1: Make a script name it CameraController, then drag drop it into Camera gameobject
    // 7.2: Move camera under GameManager children
    // 7.3: Declare a transform field name target, then drag player gameobject into it.
    // 7.4: Let write down some script for camera to follow target
    // 7.5: Introduce Tag System, GameObject.FindGameObjectWithTag API and Update method
    // 8. Intentionally make code go wrong for false test example.
    // 8.1 Pause game and explain camera and target position
    // 8.2 Fix the following target code
    // 9. Try again

    //III. Create dots
    // 1: create another gameobject which similar to player but it have different color, and has smaller size. Name it Dot
    // 2: make it prefab, explain what is prefab, Then delete it in scene.
    // 3: create a monobehaviour script, name it DotsSpawner, explain what it's purpose
    // 4: Inside GameManager gameobject, create a child gameobject name it DotsSpawner then drag drop DotsSpawner script into it
    // 5: Let create a simple method for spawning a single dot
    // 5.1: Declare a gameobject field in DotsPawner to hold Dot Prefab. Name it dotPrefab
    // 5.2: Delcare a vector 2 field, which is a size of square that all dots wll be spawn inside it. Name it spawnRange.
    // 5.3: Declare another integer field, name it totalDots. Which is total number of dots we will spawn
    // 5.4: Instantiate dot gameobjects with random position inside spawnRange
    // 5.5: Drag and drop prefab into DotsSpaner Inspector, set spawnRange equal to 50, 50, Set totalDots equal to 100. Then press Play
    // 6: Introduce OnDrawGizmos to draw spawnRange in editor
    // 7: Try to create 1000 dots. 
    // 8: Challenge 1: Try using coroutine to create 1000 dots, delay 0.1 second for each spawn.
    //--PAUSE NOTICE
    // 9: Show code
    // 10: Challenge 2: create 2 more dots, name it BlueDot and RedDot. Then modify DotsSpawner script to spawn randomly three dots
    //--PAUSE NOTICE
    // 11. Show code

    //IV. Basic Gameplay player move and collider with dots
    // 1: Introduce collision system
    // 2: Add Collider2D component to player and Dot prefab
    // 3: Add RigidBody2D component to player and Dot prefab, explain gravity scale, then set it equal to 0
    // 4: Play and show how objects contact together
    // 5: Use Rigidbody2D to move player, then Play again to show the difference between move by rigidbody and move by transform
    // 5.1 Declare a boolean field in PlayerMovement. This field is used in update method to decide what movement method should be used 
    // 6: Introduction OnCollisionEnter2D
    // 6.1: Log the name of collidered object in OnCollisionEnter2D method
    // 7: Introduction Is Trigger and OnTriggerEnter2D method
    // 7.1: Log the name of object in OnTriggerEnter2D method
    // 8: Add a simple gameplay where player eat dot and get bigger
    // 8.1: Check Trigger with Dot by tag, destroy dot when it get eaten, add one score to player
    // 8.2: Add new tag Dot, and used it for DotPrefab
    // 8.3: Make Player bigger when it eats dots
    // 9. Play and watch result

    //V: Score system
    // 1. Create a script call, DotController then add it to DotPrefab
    // 2. Declare a integer field to define score name it bonusScore of Dot in DotController.
    // 3. Introduce GetComponent API. And use it in OnTriggerEnter2D to get score from Dot. Then add score to Player.
    // 4. Challenge: Modify red dot will minus score, while Blue dot increase the move speed.
    //--PAUSE NOTICE
    // 5. Show code

    //VI: Singleton Pattern
    // 1. Explain singleton, use Singleton in GameManager
    // 2. Move score of player to GameManager, then use singleton to add score
    // 3. Challenge: Display score in UI, use the UI knowledge from previous section.
    //--PAUSE NOTICE
    // 4. Show code

    //VII. Pooling Object
    // 1. Explain pooling object
    // 2. Modify create method in DotsSpawner to use Pooling Objects
    // 2.1: Create a simple pool with two list active list and in active list (active list contain active gameobject while inactive list contain inactive gameobject)
    // 2.2: Define DotsSpawner in GameManager then drag drop DotsSpawner to it
    // 2.3: Replace creat/destroy code by spawn/release code

    //VIII: Game UI
    // 1. We already have game states, now use this to manage action of other objects.
    // 1.1: When game start we active player and DotsSpawner
    // 1.2: Player and DotsSpawner only active when game state is GamePlay
    // 1.3: Dont allow player move if not active
    // 1.4: Dont allow DotsSpawner spawns dots if not active
    // 1.5: Add a method to clear dots to pool and use it in DotsSpawner. Clear dots when game start
    // 2. Add time count down to GameManager. when time is out, change state to gameover and display GameoverPanel
    // 3. Display countdown in the UI.
    // 3.1: Refresh time in Update method
    // 3.2: Play, Then explain it is not healthy way
    // 4. Use coroutine to refresh time
    // 4.1: Play, then explain why it is more healthy

    //IX: Highscore and playerPref
    // 1. Introduce playerPref
    // 2. Use playerPref to save score
    // 2.1 Create a static class name it GameData to save data
    // 2.2 Ingame GameData declare playerPref HighScore property
    // 2.3 Explain how it work
    // 3. Use playerPref to save high score
    // 4. Challenge: Display high score in GameoverPanel
    //-- PAUSE NOTICE
    // 5. Show code

    //X: Obsever Pattern and create radar
    //1. Introduce Obsever Pattern
    //2. Introduce idea of radar
    //3. Create radar
    //4. Create dispatcher in gamemanager, listener in radar

    public class PlayerMovement : MonoBehaviour
    {
        public float moveSpeed;
        public Rigidbody2D rigid2d;
        public bool moveByRigid;
        //public int score;
        public bool active;

        private void Update()
        {
            if (!active)
                return;

            float vertical = Input.GetAxis("Vertical");
            float horizontal = Input.GetAxis("Horizontal");
            float x = horizontal * moveSpeed * Time.deltaTime;
            float y = vertical * moveSpeed * Time.deltaTime;

            if (moveByRigid)
                rigid2d.velocity = new Vector2(x, y);
            else
                transform.Translate(new Vector2(x, y));
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            Debug.Log("Collider With " + collision.gameObject.name);
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            Debug.Log("Trigger With " + collision.gameObject.name);

            if (collision.CompareTag("Dot"))
            {
                DotController dot = collision.GetComponent<DotController>();

                //score += dot.bonusScore;
                //GameManager.instance.score += dot.bonusScore;
                GameManager.instance.AddScore(dot.bonusScore);
                moveSpeed += dot.bonusSpeed;
                transform.localScale = Vector3.one * (1 + GameManager.instance.score / 100f);

                //Destroy(collision.gameObject);
                GameManager.instance.dotsSpawner.ReleaseDot(dot.gameObject);
            }
        }

        public void Active(bool pValue)
        {
            active = pValue;
        }
    }
}