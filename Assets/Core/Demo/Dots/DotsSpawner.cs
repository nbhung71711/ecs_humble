﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Section3
{
    [System.Serializable]
    public class SimplePool
    {
        public GameObject prefab;
        public List<GameObject> activeDots;
        public List<GameObject> inactiveDots;
        public GameObject Spawn(Vector2 position)
        {
            if (inactiveDots.Count > 0)
            {
                GameObject dot = inactiveDots[0];
                dot.SetActive(true);
                dot.transform.position = position;
                inactiveDots.Remove(dot);
                activeDots.Add(dot);
                return dot;
            }
            else
            {
                GameObject dot = GameObject.Instantiate(prefab, position, Quaternion.identity);
                activeDots.Add(dot);
                return dot;
            }
        }
        public void Release(GameObject obj)
        {
            if (activeDots.Contains(obj))
            {
                obj.SetActive(false);
                activeDots.Remove(obj);
                inactiveDots.Add(obj);
            }
        }
        public void Clear()
        {
            while (activeDots.Count > 0)
            {
                GameObject obj = activeDots[0];
                obj.SetActive(false);
                activeDots.Remove(obj);
                inactiveDots.Add(obj);
            }
        }
    }

    public class DotsSpawner : MonoBehaviour
    {
        public GameObject[] dotPrefabs;
        public SimplePool[] dotPools;
        public Vector2 spawnRange;
        public int totalDots;
        public bool active;

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireCube(transform.position, spawnRange);
        }

        private void CreateDots()
        {
            for (int i = 0; i < totalDots; i++)
            {
                float x = UnityEngine.Random.Range(-spawnRange.x / 2f, spawnRange.x / 2f);
                float y = UnityEngine.Random.Range(-spawnRange.y / 2f, spawnRange.y / 2f);

                //GameObject dotPrefab = dotPrefabs[Random.Range(0, dotPrefabs.Length)];
                //Instantiate(dotPrefab, new Vector2(x, y), Quaternion.identity);

                SimplePool dotPool = dotPools[UnityEngine.Random.Range(0, dotPools.Length)];
                dotPool.Spawn(new Vector2(x, y));
            }
        }

        private IEnumerator IECreateDots()
        {
            var delay = new WaitForSeconds(0.1f);
            for (int i = 0; i < totalDots; i++)
            {
                while (!active)
                    yield return null;

                float x = UnityEngine.Random.Range(-spawnRange.x / 2f, spawnRange.x / 2f);
                float y = UnityEngine.Random.Range(-spawnRange.y / 2f, spawnRange.y / 2f);

                //GameObject dotPrefab = dotPrefabs[Random.Range(0, dotPrefabs.Length)];
                //Instantiate(dotPrefab, new Vector2(x, y), Quaternion.identity);

                SimplePool dotPool = dotPools[UnityEngine.Random.Range(0, dotPools.Length)];
                dotPool.Spawn(new Vector2(x, y));

                yield return delay;
            }
        }

        public void ReleaseDot(GameObject gameObject)
        {
            for (int i = 0; i < dotPools.Length; i++)
                dotPools[i].Release(gameObject);
        }

        public void Active(bool pValue)
        {
            active = pValue;
        }

        public void StartGame()
        {
            ClearDots();

            //CreateDots();

            StartCoroutine(IECreateDots());
        }

        private void ClearDots()
        {
            for (int i = 0; i < dotPools.Length; i++)
                dotPools[i].Clear();
        }
    }
}