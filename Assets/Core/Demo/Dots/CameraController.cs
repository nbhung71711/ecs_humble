﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Section3
{
    public class CameraController : MonoBehaviour
    {
        public GameObject target;

        private void Start()
        {
            target = GameObject.FindGameObjectWithTag("Player");
        }

        private void Update()
        {
            if (target != null)
            {
                Vector3 targetPosition = target.transform.position;
                targetPosition.z = -10;
                transform.position = targetPosition;
            }
        }
    }
}