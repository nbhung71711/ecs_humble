﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities.Common;
using Debug = Utilities.Common.Debug;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Utilities.Components
{
    public class AudioSFX : MonoBehaviour
    {
        [SerializeField] private AudioClip[] mClips;
        [SerializeField] private bool mIsLoop;
        [SerializeField, Range(0.5f, 2f)] private float mPitchRandomMultiplier = 1f;
        [SerializeField, Range(1, 5)] private int mLimitNumber = 1;

        public void PlaySFX()
        {
            if (mClips.Length > 0)
            {
                var clip = mClips[Random.Range(0, mClips.Length)];
                AudioManager.Instance?.PlaySFX(clip, mLimitNumber, mIsLoop, mPitchRandomMultiplier);
            }
        }

        public void StopSFX()
        {
            for (int i = 0; i < mClips.Length; i++)
                HybirdAudioManager.Instance?.StopSFX(mClips[i]);
        }

#if UNITY_EDITOR
        [CustomEditor(typeof(AudioSFX))]
        private class AudioSFXEditor : Editor
        {
            private AudioSFX mScript;
            private string mSearch = "";

            private void OnEnable()
            {
                mScript = target as AudioSFX;

                if (mScript.mClips == null)
                    mScript.mClips = new AudioClip[0];
            }

            public override void OnInspectorGUI()
            {
                base.OnInspectorGUI();

                if (AudioCollection.Instance == null)
                {
                    if (AudioCollection.Instance == null)
                        EditorGUILayout.HelpBox("AudioSFX require AudioCollection. " +
                            "To create AudioCollection, select Resources folder then from Create Menu " +
                            "select RUtilities/Create Audio Collection", MessageType.Error);
                    return;
                }

                if (mScript.mClips.Length > 0)
                    EditorHelper.BoxVertical(() =>
                    {
                        for (int i = 0; i < mScript.mClips.Length; i++)
                        {
                            EditorHelper.BoxHorizontal(() =>
                            {
                                EditorHelper.ObjectField<AudioClip>(mScript.mClips[i], "");
                                if (EditorHelper.ButtonColor("x", Color.red, 24))
                                {
                                    var list = mScript.mClips.ToList();
                                    list.Remove(mScript.mClips[i]);
                                    mScript.mClips = list.ToArray();
                                }
                            });
                        }
                    }, Color.white, true);

                EditorHelper.BoxVertical(() =>
                {
                    mSearch = EditorHelper.TextField(mSearch, "Search");
                    if (!string.IsNullOrEmpty(mSearch))
                    {
                        var clips = AudioCollection.Instance.sfxClips;
                        for (int i = 0; i < clips.Length; i++)
                        {
                            if (clips[i].name.ToLower().Contains(mSearch.ToLower()))
                            {
                                if (GUILayout.Button(clips[i].name))
                                {
                                    var list = mScript.mClips.ToList();
                                    if (!list.Contains(clips[i]))
                                    {
                                        list.Add(clips[i]);
                                        mScript.mClips = list.ToArray();
                                    }
                                }
                            }
                        }
                    }
                }, Color.white, true);

                if (EditorHelper.ButtonColor("Open Sounds Collection"))
                    Selection.activeObject = AudioCollection.Instance;

                if (GUI.changed)
                    EditorUtility.SetDirty(mScript);
            }
        }
#endif
    }
}
