﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities.Common;
using Debug = Utilities.Common.Debug;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Utilities.Components
{
    public class HybirdAudioSFX : MonoBehaviour
    {
        [SerializeField] private string[] mIdStrings;
        [SerializeField] private bool mIsLoop;
        [SerializeField, Range(0.5f, 2f)] private float mPitchRandomMultiplier = 1f;
        private HybirdAudioCollection.Clip[] mSounds;
        private bool mInitialized;

        private void Start()
        {
            Init();
        }

        private void Init()
        {
            if (mInitialized) return;
            mSounds = new HybirdAudioCollection.Clip[mIdStrings.Length];
            for (int i = 0; i < mIdStrings.Length; i++)
            {
                var sound = HybirdAudioCollection.Instance.GetClip(mIdStrings[i], false);
                if (sound == null)
                    Debug.LogError("Not found SFX id " + mIdStrings[i]);
                mSounds[i] = sound;
            }
            mInitialized = true;
        }

        public void PlaySFX()
        {
            Init();

            if (mSounds.Length > 0)
            {
                var sound = mSounds[Random.Range(0, mSounds.Length)];
                HybirdAudioManager.Instance?.PlaySFX(sound.clip, sound.limitNumber, mIsLoop, mPitchRandomMultiplier);
            }
        }

        public void StopSFX()
        {
            for (int i = 0; i < mSounds.Length; i++)
                HybirdAudioManager.Instance?.StopSFX(mSounds[i].clip);
        }

#if UNITY_EDITOR
        [CustomEditor(typeof(HybirdAudioSFX))]
        private class HybirdAudioSFXEditor : Editor
        {
            private HybirdAudioSFX mScript;
            private string mSearch = "";

            private void OnEnable()
            {
                mScript = target as HybirdAudioSFX;

                if (mScript.mIdStrings == null)
                    mScript.mIdStrings = new string[0] { };
            }

            public override void OnInspectorGUI()
            {
                base.OnInspectorGUI();

                if (HybirdAudioCollection.Instance == null)
                {
                    if (HybirdAudioCollection.Instance == null)
                        EditorGUILayout.HelpBox("HybirdAudioSFX require HybirdAudioCollection. " +
                            "To create HybirdAudioCollection,select Resources folder then from Create Menu " +
                            "select RUtilities/Create Hybird Audio Collection", MessageType.Error);
                    return;
                }

                if (mScript.mIdStrings.Length > 0)
                    EditorHelper.BoxVertical(() =>
                    {
                        for (int i = 0; i < mScript.mIdStrings.Length; i++)
                        {
                            EditorHelper.BoxHorizontal(() =>
                            {
                                EditorHelper.TextField(mScript.mIdStrings[i], "SoundId");
                                if (EditorHelper.ButtonColor("x", Color.red, 24))
                                {
                                    var list = mScript.mIdStrings.ToList();
                                    list.Remove(mScript.mIdStrings[i]);
                                    mScript.mIdStrings = list.ToArray();
                                }
                            });
                        }
                    }, Color.white, true);

                EditorHelper.BoxVertical(() =>
                {
                    mSearch = EditorHelper.TextField(mSearch, "Search");
                    if (!string.IsNullOrEmpty(mSearch))
                    {
                        var sounds = HybirdAudioCollection.Instance.SFXClips;
                        for (int i = 0; i < sounds.Count; i++)
                        {
                            if (sounds[i].fileName.ToLower().Contains(mSearch.ToLower()))
                            {
                                if (GUILayout.Button(sounds[i].fileName))
                                {
                                    var list = mScript.mIdStrings.ToList();
                                    if (!list.Contains(sounds[i].fileName))
                                    {
                                        list.Add(sounds[i].fileName);
                                        mScript.mIdStrings = list.ToArray();
                                    }
                                }
                            }
                        }
                    }
                }, Color.white, true);

                //if (EditorHelper.ButtonColor("Open Sounds Collection"))
                //    Selection.activeObject = HybirdAudioCollection.Instance;

                //if (EditorHelper.ContainDirective("HYBIRD_AUDIO_MANAGER"))
                //{
                //    if (EditorHelper.ButtonColor("Deactive Hybird Audio Manager", Color.red))
                //        EditorHelper.RemoveDirective("HYBIRD_AUDIO_MANAGER");
                //}
                //else if (EditorHelper.ButtonColor("Active Hybird Audio Manager"))
                //{
                //    if (EditorHelper.ContainDirective("AUDIO_MANAGER"))
                //        EditorHelper.RemoveDirective("AUDIO_MANAGER");
                //    if (EditorHelper.ButtonColor("Active Hybird Audio Manager", Color.green))
                //        EditorHelper.AddDirective("HYBIRD_AUDIO_MANAGER");
                //}

                if (GUI.changed)
                    EditorUtility.SetDirty(mScript);
            }
        }
#endif
    }
}