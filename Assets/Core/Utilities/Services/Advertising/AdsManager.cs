﻿#pragma warning disable 0649
/**
 * Author NBear - nbhung71711 @gmail.com - 2019
 * Based on Ads library from Easy Mobile Plugin
 **/
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Utilities.Service.Ads
{
    public class AdsManager : MonoBehaviour
    {
        private static AdsManager mInstance;
        public static AdsManager Instance
        {
            get
            {
                if (mInstance == null)
                {
                    mInstance = FindObjectOfType<AdsManager>();
                    if (mInstance == null)
                    {
                        var obj = new GameObject("Advertising");
                        mInstance = obj.AddComponent<AdsManager>();
                        DontDestroyOnLoad(obj);
                    }
                }
                return mInstance;
            }
        }

        [SerializeField] private bool mAutoInit = true;

        [Header("Android Default Network")]
        [SerializeField] private BannerAdNetwork mAndroidDefaultBannerAdNetwork;
        [SerializeField] private InterstitialAdNetwork mAndroidDefaultInterstitialAdNetwork;
        [SerializeField] private RewardedAdNetwork mAndroidDefaultRewardedAdNetwork;

        [Header("IOS Default Network")]
        [SerializeField] private BannerAdNetwork miOSDefaultBannerAdNetwork;
        [SerializeField] private InterstitialAdNetwork miOSDefaultInterstitialAdNetwork;
        [SerializeField] private RewardedAdNetwork miOSDefaultRewardedAdNetwork;

#if !ACTIVE_ADMOB
        [HideInInspector]
#endif
        [Header("Admob"), SerializeField] private AdMobSettings mAdMobSettings;
#if !UNITY_ADS
        [HideInInspector]
#endif
        [Header("Unity Ads"), SerializeField] private UnityAdsSettings mUnityAdSettings;
#if !ACTIVE_FBAN
        [HideInInspector]
#endif
        [Header("FAN"), SerializeField] private AudienceNetworkSettings mAudienceNetworkSettings;
#if !ACTIVE_TAPJOY
        [HideInInspector]
#endif
        [Header("TapJoy"), SerializeField] private TapjoySettings mTapJoySettings;
#if !ACTIVE_IRONSOURCE
        [HideInInspector]
#endif
        [Header("IronSource"), SerializeField] private IronSourceSettings mIronSourceSettings;
        [Space(10)]
        [SerializeField, Tooltip("Hours"), Range(1, 24)] private int mResetAdsWeightAfter = 12;

        // Supported ad clients.
        private AdMobClient mAdMobClient;
        private AudienceNetworkClient mAudienceNetworkClient;
        private TapjoyClient mTapjoyClient;
        private UnityAdsClient mUnityAdsClient;
        private IronSourceClient mIronSourceClient;

        // Default ad clients for each ad types.
        private AdClient mBannerAdClient;
        private AdClient mInterstitialAdClient;
        private AdClient mRewardedAdClient;

        #region Ad Events

        public event Action<InterstitialAdNetwork> onInterstitialAdCompleted;
        public event Action<RewardedAdNetwork> onRewardedAdSkipped;
        public event Action<RewardedAdNetwork> onRewardedAdCompleted;

        #endregion

        #region Ad Clients

        public AdMobClient AdMobClient
        {
            get
            {
                if (mAdMobClient == null)
                {
                    mAdMobClient = CreateAdClient(AdNetwork.AdMob) as AdMobClient;
                    mAdMobClient.Init(Instance.mAdMobSettings);
                }
                return mAdMobClient;
            }
        }
        public AudienceNetworkClient AudienceNetworkClient
        {
            get
            {
                if (mAudienceNetworkClient == null)
                {
                    mAudienceNetworkClient = CreateAdClient(AdNetwork.AudienceNetwork) as AudienceNetworkClient;
                    mAudienceNetworkClient.Init(Instance.mAudienceNetworkSettings);
                }
                return mAudienceNetworkClient;
            }
        }
        public TapjoyClient TapjoyClient
        {
            get
            {
                if (mTapjoyClient == null)
                {
                    mTapjoyClient = CreateAdClient(AdNetwork.TapJoy) as TapjoyClient;
                    mTapjoyClient.Init(Instance.mTapJoySettings);
                }
                return mTapjoyClient;
            }
        }
        public UnityAdsClient UnityAdsClient
        {
            get
            {
                if (mUnityAdsClient == null)
                {
                    mUnityAdsClient = CreateAdClient(AdNetwork.UnityAds) as UnityAdsClient;
                    mUnityAdsClient.Init(Instance.mUnityAdSettings);
                }
                return mUnityAdsClient;
            }
        }
        public IronSourceClient IronSourceClient
        {
            get
            {
                if (mIronSourceClient == null)
                {
                    mIronSourceClient = CreateAdClient(AdNetwork.IronSource) as IronSourceClient;
                    mIronSourceClient.Init(Instance.mIronSourceSettings);
                }
                return mIronSourceClient;
            }
        }

        private AdClient DefaultBannerAdClient
        {
            get
            {
                if (mBannerAdClient == null)
                {
                    switch (Application.platform)
                    {
                        case RuntimePlatform.Android:
                            mBannerAdClient = GetWorkableAdClient((AdNetwork)Instance.mAndroidDefaultBannerAdNetwork);
                            break;
                        case RuntimePlatform.IPhonePlayer:
                            mBannerAdClient = GetWorkableAdClient((AdNetwork)Instance.miOSDefaultBannerAdNetwork);
                            break;
                        default:
                            mBannerAdClient = GetWorkableAdClient(AdNetwork.None);
                            break;
                    }
                }
                return mBannerAdClient;
            }
        }
        private AdClient DefaultInterstitialAdClient
        {
            get
            {
                if (mInterstitialAdClient == null)
                {
                    switch (Application.platform)
                    {
                        case RuntimePlatform.Android:
                            mInterstitialAdClient = GetWorkableAdClient((AdNetwork)Instance.mAndroidDefaultInterstitialAdNetwork);
                            break;
                        case RuntimePlatform.IPhonePlayer:
                            mInterstitialAdClient = GetWorkableAdClient((AdNetwork)Instance.miOSDefaultInterstitialAdNetwork);
                            break;
                        case RuntimePlatform.WindowsEditor:
                        case RuntimePlatform.OSXEditor:
                            mInterstitialAdClient = GetWorkableAdClient(AdNetwork.UnityAds);
                            break;
                        default:
                            mInterstitialAdClient = GetWorkableAdClient(AdNetwork.None);
                            break;
                    }
                }
                return mInterstitialAdClient;
            }
        }
        private AdClient DefaultRewardedAdClient
        {
            get
            {
                if (mRewardedAdClient == null)
                {
                    switch (Application.platform)
                    {
                        case RuntimePlatform.Android:
                            mRewardedAdClient = GetWorkableAdClient((AdNetwork)Instance.mAndroidDefaultRewardedAdNetwork);
                            break;
                        case RuntimePlatform.IPhonePlayer:
                            mRewardedAdClient = GetWorkableAdClient((AdNetwork)Instance.miOSDefaultRewardedAdNetwork);
                            break;
                        case RuntimePlatform.WindowsEditor:
                        case RuntimePlatform.OSXEditor:
                            mRewardedAdClient = GetWorkableAdClient(AdNetwork.UnityAds);
                            break;
                        default:
                            mRewardedAdClient = GetWorkableAdClient(AdNetwork.None);
                            break;
                    }
                }
                return mRewardedAdClient;
            }
        }

        #endregion

        #region MonoBehaviour Events

        private void Awake()
        {
            if (mInstance == null)
            {
                mInstance = this;
                DontDestroyOnLoad(gameObject);
            }
            else if (mInstance != this)
                Destroy(this);
        }

        private void Start()
        {
            if (mAutoInit)
                Init();
        }

        #endregion

        #region Ads API

        //------------------------------------------------------------
        // Banner Ads.
        //------------------------------------------------------------

        public void Init()
        {
#if ACTIVE_ADMOB
            mAdMobClient = CreateAdClient(AdNetwork.AdMob) as AdMobClient;
            mAdMobClient.Init(Instance.mAdMobSettings);
#endif
#if ACTIVE_FBAN
            mAudienceNetworkClient = CreateAdClient(AdNetwork.AudienceNetwork) as AudienceNetworkClient;
            mAudienceNetworkClient.Init(Instance.mAudienceNetworkSettings);
#endif
#if ACTIVE_TAPJOY
            mTapjoyClient = CreateAdClient(AdNetwork.TapJoy) as TapjoyClient;
            mTapjoyClient.Init(Instance.mTapJoySettings);
#endif
#if UNITY_ADS
            mUnityAdsClient = CreateAdClient(AdNetwork.UnityAds) as UnityAdsClient;
            mUnityAdsClient.Init(Instance.mUnityAdSettings);
#endif
#if ACTIVE_IRONSOURCE
            mIronSourceClient = CreateAdClient(AdNetwork.IronSource) as IronSourceClient;
            mIronSourceClient.Init(Instance.mIronSourceSettings);
#endif
            RuntimeHelper.Init();
            CheckToResetAdsWeight();
            StartCoroutine(IERAutoLoadAllAds(5f, 60f));
        }

        public void SetUnityAdsSettings(UnityAdsSettings pSettings)
        {
            mUnityAdSettings = pSettings;
        }

        public void SetAdMobSettings(AdMobSettings pSettings)
        {
            mAdMobSettings = pSettings;
        }

        public void SetTapJoySettings(TapjoySettings pSettings)
        {
            mTapJoySettings = pSettings;
        }

        public void SetAudienceNetworkSettings(AudienceNetworkSettings pSettings)
        {
            mAudienceNetworkSettings = pSettings;
        }

        public void SetIronSourceSettings(IronSourceSettings pSettings)
        {
            mIronSourceSettings = pSettings;
        }

        /// <summary>
        /// Shows the default banner ad at the specified position.
        /// </summary>
        public void ShowBannerAd(BannerAdPosition position)
        {
            ShowBannerAd(DefaultBannerAdClient, position, BannerAdSize.SmartBanner);
        }

        /// <summary>
        /// Shows a banner ad of the default banner ad network
        /// at the specified position and size.
        /// </summary>
        public void ShowBannerAd(BannerAdPosition position, BannerAdSize size)
        {
            ShowBannerAd(DefaultBannerAdClient, position, size);
        }

        /// <summary>
        /// Shows a banner ad of the specified ad network at the specified position and size.
        /// </summary>
        public void ShowBannerAd(BannerAdNetwork adNetwork, BannerAdPosition position, BannerAdSize size)
        {
            ShowBannerAd(GetWorkableAdClient((AdNetwork)adNetwork), position, size);
        }

        /// <summary>
        /// Hides the default banner ad.
        /// </summary>
        public void HideBannerAd()
        {
            HideBannerAd(DefaultBannerAdClient);
        }

        /// <summary>
        /// Hides the banner ad of the specified ad network .
        /// </summary>
        public void HideBannerAd(BannerAdNetwork adNetwork)
        {
            HideBannerAd(GetWorkableAdClient((AdNetwork)adNetwork));
        }

        /// <summary>
        /// Destroys the default banner ad.
        /// </summary>
        public void DestroyBannerAd()
        {
            DestroyBannerAd(DefaultBannerAdClient);
        }

        /// <summary>
        /// Destroys the banner ad of the specified ad network .
        /// </summary>
        public void DestroyBannerAd(BannerAdNetwork adNetwork)
        {
            DestroyBannerAd(GetWorkableAdClient((AdNetwork)adNetwork));
        }

        //------------------------------------------------------------
        // Interstitial Ads.
        //------------------------------------------------------------

        /// <summary>
        /// Loads the default interstitial ad.
        /// </summary>
        public void LoadInterstitialAd()
        {
            LoadInterstitialAd(DefaultInterstitialAdClient);
        }

        /// <summary>
        /// Loads the interstitial ad of the specified ad network .
        /// </summary>
        public void LoadInterstitialAd(InterstitialAdNetwork adNetwork)
        {
            LoadInterstitialAd(GetWorkableAdClient((AdNetwork)adNetwork));
        }

        /// <summary>
        /// Determines whether the default interstitial ad is ready to show.
        /// </summary>
        public bool IsInterstitialAdReady()
        {
            return IsInterstitialAdReady(DefaultInterstitialAdClient);
        }

        /// <summary>
        /// Determines whether the interstitial ad of the specified ad network 
        ///  is ready to show.
        /// </summary>
        public bool IsInterstitialAdReady(InterstitialAdNetwork adNetwork)
        {
            return IsInterstitialAdReady(GetWorkableAdClient((AdNetwork)adNetwork));
        }

        /// <summary>
        /// Shows the default interstitial ad.
        /// </summary>
        public void ShowInterstitialAd()
        {
            ShowInterstitialAd(DefaultInterstitialAdClient);
        }

        /// <summary>
        /// Shows the interstitial ad of the specified ad network.
        /// </summary>
        public void ShowInterstitialAd(InterstitialAdNetwork adNetwork)
        {
            ShowInterstitialAd(GetWorkableAdClient((AdNetwork)adNetwork));
        }

        /// <summary>
        /// Show random interstitial ad, depend on the weight number of each network type
        /// </summary>
        public void ShowInterstitialAdRandomly()
        {
            var networkChances = new List<int>();
            var networks = new List<InterstitialAdNetwork>();
            if (mAdMobClient != null && mAdMobClient.IsInterstitialAdReady())
            {
                int weight = mAdMobSettings.CurInterstitialAdWeight * 100;
                weight = weight <= 0 ? 10 : weight;
                networkChances.Add(weight);
                networks.Add(InterstitialAdNetwork.AdMob);
            }
            if (mAudienceNetworkClient != null && mAudienceNetworkClient.IsInterstitialAdReady())
            {
                int weight = mAudienceNetworkSettings.CurInterstitialAdWeight * 100;
                weight = weight <= 0 ? 10 : weight;
                networkChances.Add(weight);
                networks.Add(InterstitialAdNetwork.AudienceNetwork);
            }
            if (mIronSourceClient != null && mIronSourceClient.IsInterstitialAdReady())
            {
                int weight = mIronSourceSettings.CurInterstitialAdWeight * 100;
                weight = weight <= 0 ? 10 : weight;
                networkChances.Add(weight);
                networks.Add(InterstitialAdNetwork.IronSource);
            }
            if (mTapjoyClient != null && mTapjoyClient.IsInterstitialAdReady())
            {
                int weight = mTapJoySettings.CurInterstitialAdWeight * 100;
                weight = weight <= 0 ? 10 : weight;
                networkChances.Add(weight);
                networks.Add(InterstitialAdNetwork.TapJoy);
            }
            if (mUnityAdsClient != null && mUnityAdsClient.IsInterstitialAdReady())
            {
                int weight = mUnityAdSettings.CurInterstitialAdWeight * 100;
                weight = weight <= 0 ? 10 : weight;
                networkChances.Add(weight);
                networks.Add(InterstitialAdNetwork.UnityAds);
            }
            if (networkChances.Count == 0)
                return;

            int totalRatio = 0;
            for (int i = 0; i < networkChances.Count; i++)
                totalRatio += networkChances[i];

            int selected = -1;
            int random = Random.Range(0, totalRatio);
            int temp = 0;
            for (int i = 0; i < networkChances.Count; i++)
            {
                temp += networkChances[i];
                if (temp > random)
                {
                    selected = i;
                    break;
                }
            }
            var network = InterstitialAdNetwork.None;
            if (selected > -1)
                network = networks[selected];

            ShowInterstitialAd(network);
        }

        //------------------------------------------------------------
        // Rewarded Ads.
        //------------------------------------------------------------

        /// <summary>
        /// Loads the default rewarded ad.
        /// </summary>
        public void LoadRewardedAd()
        {
            LoadRewardedAd(DefaultRewardedAdClient);
        }

        /// <summary>
        /// Loads the rewarded ad of the specified ad network.
        /// </summary>
        public void LoadRewardedAd(RewardedAdNetwork adNetwork)
        {
            LoadRewardedAd(GetWorkableAdClient((AdNetwork)adNetwork));
        }

        /// <summary>
        /// Determines whether the default rewarded ad is ready to show.
        /// </summary>
        public bool IsRewardedAdReady()
        {
            return IsRewardedAdReady(DefaultRewardedAdClient);
        }

        public bool IsRewardedAdReady(RewardedAdNetwork adNetwork)
        {
            return IsRewardedAdReady(GetWorkableAdClient((AdNetwork)adNetwork));
        }

        /// <summary>
        /// Shows the default rewarded ad.
        /// </summary>
        public void ShowRewardedAd()
        {
            ShowRewardedAd(DefaultRewardedAdClient);
        }

        /// <summary>
        /// Shows the rewarded ad of the specified ad network .
        /// </summary>
        public void ShowRewardedAd(RewardedAdNetwork adNetwork)
        {
            ShowRewardedAd(GetWorkableAdClient((AdNetwork)adNetwork));
        }

        /// <summary>
        /// Show random interstitial ad, depend on the weight number of each network type
        /// </summary>
        public void ShowRewardedAdRandomly()
        {
            var networkChances = new List<int>();
            var networks = new List<RewardedAdNetwork>();
            if (mAdMobClient != null && mAdMobClient.IsRewardedAdReady())
            {
                int weight = mAdMobSettings.CurRewardedAdWeight * 100;
                weight = weight <= 0 ? 10 : weight;
                networkChances.Add(weight);
                networks.Add(RewardedAdNetwork.AdMob);
            }
            if (mAudienceNetworkClient != null && mAudienceNetworkClient.IsRewardedAdReady())
            {
                int weight = mAudienceNetworkSettings.CurRewardedAdWeight * 100;
                weight = weight <= 0 ? 10 : weight;
                networkChances.Add(weight);
                networks.Add(RewardedAdNetwork.AudienceNetwork);
            }
            if (mIronSourceClient != null && mIronSourceClient.IsRewardedAdReady())
            {
                int weight = mIronSourceSettings.CurRewardedAdWeight * 100;
                weight = weight <= 0 ? 10 : weight;
                networkChances.Add(weight);
                networks.Add(RewardedAdNetwork.IronSource);
            }
            if (mTapjoyClient != null && mTapjoyClient.IsRewardedAdReady())
            {
                int weight = mTapJoySettings.CurRewardedAdWeight * 100;
                weight = weight <= 0 ? 10 : weight;
                networkChances.Add(weight);
                networks.Add(RewardedAdNetwork.TapJoy);
            }
            if (mUnityAdsClient != null && mUnityAdsClient.IsRewardedAdReady())
            {
                int weight = mUnityAdSettings.CurRewardedAdWeight * 100;
                weight = weight <= 0 ? 10 : weight;
                networkChances.Add(weight);
                networks.Add(RewardedAdNetwork.UnityAds);
            }
            if (networkChances.Count == 0)
                return;

            int totalRatio = 0;
            for (int i = 0; i < networkChances.Count; i++)
                totalRatio += networkChances[i];

            int selected = -1;
            int random = Random.Range(0, totalRatio);
            int temp = 0;
            for (int i = 0; i < networkChances.Count; i++)
            {
                temp += networkChances[i];
                if (temp > random)
                {
                    selected = i;
                    break;
                }
            }
            var network = RewardedAdNetwork.None;
            if (selected > -1)
                network = networks[selected];

            ShowRewardedAd(network);
        }

        #endregion // Public API

        #region Internal Stuff

        private void CheckToResetAdsWeight()
        {
            string lastDateString = PlayerPrefs.GetString("lastDateOfAds");
            DateTime lateDate = DateTime.MinValue;
            DateTime.TryParse(lastDateString, out lateDate);

            if ((DateTime.Now - lateDate).TotalHours > mResetAdsWeightAfter)
            {
                mAdMobSettings.ResetCurWeight();
                mAudienceNetworkSettings.ResetCurWeight();
                mIronSourceSettings.ResetCurWeight();
                mTapJoySettings.ResetCurWeight();
                mUnityAdSettings.ResetCurWeight();
                PlayerPrefs.SetString("lastDateOfAds", DateTime.Now.ToString());
            }
        }

        private void ShowBannerAd(IAdClient client, BannerAdPosition position, BannerAdSize size)
        {
            client.ShowBannerAd(position, size);
        }

        private void HideBannerAd(IAdClient client)
        {
            client.HideBannerAd();
        }

        private void DestroyBannerAd(IAdClient client)
        {
            client.DestroyBannerAd();
        }

        private void LoadInterstitialAd(IAdClient client)
        {
            client.LoadInterstitialAd();
        }

        private bool IsInterstitialAdReady(IAdClient client)
        {
            return client.IsInterstitialAdReady();
        }

        private void ShowInterstitialAd(IAdClient client)
        {
            client.ShowInterstitialAd();
        }

        // Note that rewarded ads should still be available after ads removal.
        // which is why we don't check if ads were removed in the following methods.

        private void LoadRewardedAd(IAdClient client)
        {
            client.LoadRewardedAd();
        }

        private bool IsRewardedAdReady(IAdClient client)
        {
            return client.IsRewardedAdReady();
        }

        private void ShowRewardedAd(IAdClient client)
        {
            client.ShowRewardedAd();
        }

        /// <summary>
        /// Grabs the singleton ad client for the specified network and performs
        /// necessary setup for it, including initializing it and subscribing to its events.
        /// </summary>
        private AdClient CreateAdClient(AdNetwork network)
        {
            AdClient client = null;

            switch (network)
            {
                case AdNetwork.AdMob:
                    client = AdMobClient.CreateClient(); break;
                case AdNetwork.AudienceNetwork:
                    client = AudienceNetworkClient.CreateClient(); break;
                case AdNetwork.TapJoy:
                    client = TapjoyClient.CreateClient(); break;
                case AdNetwork.UnityAds:
                    client = UnityAdsClient.CreateClient(); break;
                case AdNetwork.None:
                    client = NoOpClient.CreateClient(); break;
                case AdNetwork.IronSource:
                    client = IronSourceClient.CreateClient(); break;
                default:
                    throw new NotImplementedException("No client implemented for the network:" + network.ToString());
            }

            if (client != null && client.Network != AdNetwork.None)
                SubscribeAdClientEvents(client);

            return client;
        }

        /// <summary>
        /// Grabs the ready to work (done initialization, setup, etc.)
        /// ad client for the specified network.
        /// </summary>
        private AdClient GetWorkableAdClient(AdNetwork network)
        {
            switch (network)
            {
                case AdNetwork.AdMob:
                    return AdMobClient;
                case AdNetwork.AudienceNetwork:
                    return AudienceNetworkClient;
                case AdNetwork.UnityAds:
                    return UnityAdsClient;
                case AdNetwork.TapJoy:
                    return TapjoyClient;
                case AdNetwork.IronSource:
                    return IronSourceClient;
                case AdNetwork.None:
                    return NoOpClient.CreateClient();
                default:
                    throw new NotImplementedException("No client found for the network:" + network.ToString());
            }
        }

        private void SubscribeAdClientEvents(IAdClient client)
        {
            if (client == null)
                return;

            client.InterstitialAdCompleted += OnInternalInterstitialAdCompleted;
            client.RewardedAdSkipped += OnInternalRewardedAdSkipped;
            client.RewardedAdCompleted += OnInternalRewardedAdCompleted;
        }

        private void OnInternalInterstitialAdCompleted(IAdClient client)
        {
            if (onInterstitialAdCompleted != null)
                onInterstitialAdCompleted((InterstitialAdNetwork)client.Network);

            switch ((InterstitialAdNetwork)client.Network)
            {
                case InterstitialAdNetwork.AdMob:
                    mAdMobSettings.CurInterstitialAdWeight -= 1;
                    break;
                case InterstitialAdNetwork.AudienceNetwork:
                    mAudienceNetworkSettings.CurInterstitialAdWeight -= 1;
                    break;
                case InterstitialAdNetwork.IronSource:
                    mIronSourceSettings.CurInterstitialAdWeight -= 1;
                    break;
                case InterstitialAdNetwork.TapJoy:
                    mTapJoySettings.CurInterstitialAdWeight -= 1;
                    break;
                case InterstitialAdNetwork.UnityAds:
                    mUnityAdSettings.CurInterstitialAdWeight -= 1;
                    break;
            }
        }

        private void OnInternalRewardedAdSkipped(IAdClient client)
        {
            if (onRewardedAdSkipped != null)
                onRewardedAdSkipped((RewardedAdNetwork)client.Network);
        }

        private void OnInternalRewardedAdCompleted(IAdClient client)
        {
            if (onRewardedAdCompleted != null)
                onRewardedAdCompleted((RewardedAdNetwork)client.Network);

            switch ((RewardedAdNetwork)client.Network)
            {
                case RewardedAdNetwork.AdMob:
                    mAdMobSettings.CurRewardedAdWeight -= 1;
                    break;
                case RewardedAdNetwork.AudienceNetwork:
                    mAudienceNetworkSettings.CurRewardedAdWeight -= 1;
                    break;
                case RewardedAdNetwork.IronSource:
                    mIronSourceSettings.CurRewardedAdWeight -= 1;
                    break;
                case RewardedAdNetwork.TapJoy:
                    mTapJoySettings.CurRewardedAdWeight -= 1;
                    break;
                case RewardedAdNetwork.UnityAds:
                    mUnityAdSettings.CurRewardedAdWeight -= 1;
                    break;
            }
        }

        /// <summary>
        /// This coroutine load all available ads (default and custom) of all imported networks.
        /// </summary>
        private IEnumerator IERAutoLoadAllAds(float delay = 0, float interval = 60)
        {
            if (delay > 0)
                yield return new WaitForSeconds(delay);

            List<IAdClient> availableInterstitialNetworks = GetAvailableNetworks<InterstitialAdNetwork>();
            List<IAdClient> availableRewardedNetworks = GetAvailableNetworks<RewardedAdNetwork>();

            while (true)
            {
                LoadAllInterstitialAds(availableInterstitialNetworks);
                LoadAllRewardedAds(availableRewardedNetworks);
                yield return new WaitForSeconds(60);
            }
        }

        /// <summary>
        /// Load all available interstitial ads of specific clients.
        /// </summary>
        private void LoadAllInterstitialAds(List<IAdClient> clients)
        {
            foreach (var client in clients)
            {
                if (!client.IsValid(AdType.Interstitial))
                    continue;

                string tempIndex = client.Network.ToString();

                if (IsInterstitialAdReady(client))
                    continue;

                LoadInterstitialAd(client);
            }
        }

        /// <summary>
        /// Load all available rewarded ads of specific clients. 
        /// </summary>
        private void LoadAllRewardedAds(List<IAdClient> clients)
        {
            foreach (var client in clients)
            {
                if (!client.IsValid(AdType.Rewarded))
                    continue;

                string tempIndex = client.Network.ToString();

                if (IsRewardedAdReady(client))
                    continue;

                LoadRewardedAd(client);
            }
        }

        /// <summary>
        /// Returns all imported ads networks.
        /// </summary>
        private List<IAdClient> GetAvailableNetworks<T>()
        {
            List<IAdClient> availableNetworks = new List<IAdClient>();
            foreach (T network in Enum.GetValues(typeof(T)))
            {
                AdClient client = GetAdClient((AdNetwork)(Enum.Parse(typeof(T), network.ToString())));
                if (client.IsSdkAvail)
                {
                    var workableClient = GetWorkableAdClient((AdNetwork)(Enum.Parse(typeof(T), network.ToString())));
                    availableNetworks.Add(workableClient);
                }
            }
            return availableNetworks;
        }

        /// <summary>
        /// Gets the singleton ad client of the specified network.
        /// This may or may not be initialized.
        /// </summary>
        private AdClient GetAdClient(AdNetwork network)
        {
            switch (network)
            {
                case AdNetwork.AdMob:
                    return AdMobClient;
                case AdNetwork.AudienceNetwork:
                    return AudienceNetworkClient;
                case AdNetwork.TapJoy:
                    return TapjoyClient;
                case AdNetwork.UnityAds:
                    return UnityAdsClient;
                case AdNetwork.IronSource:
                    return IronSourceClient;
                case AdNetwork.None:
                    return NoOpClient.CreateClient();
                default:
                    throw new NotImplementedException("No client implemented for the network:" + network.ToString());
            }
        }

        #endregion

#if UNITY_EDITOR

        #region EDitor

        [CustomEditor(typeof(AdsManager))]
        public class AdvertisingEditor : Editor
        {
            private void RemoveDirective(string pSymbol)
            {
                var taget = EditorUserBuildSettings.selectedBuildTargetGroup;
                string directives = PlayerSettings.GetScriptingDefineSymbolsForGroup(taget);
                directives = directives.Replace(pSymbol, "");
                if (directives.Length > 0 && directives[directives.Length - 1] == ';')
                    directives = directives.Remove(directives.Length - 1, 1);
                PlayerSettings.SetScriptingDefineSymbolsForGroup(taget, directives);
            }

            private void AddDirective(string pSymbol)
            {
                var taget = EditorUserBuildSettings.selectedBuildTargetGroup;
                string directives = PlayerSettings.GetScriptingDefineSymbolsForGroup(taget);
                if (string.IsNullOrEmpty(directives))
                    directives += pSymbol;
                else
                    directives += ";" + pSymbol;
                PlayerSettings.SetScriptingDefineSymbolsForGroup(taget, directives);
            }

            public override void OnInspectorGUI()
            {
                base.OnInspectorGUI();

                Color defaultColor = GUI.backgroundColor;

                GUILayout.BeginVertical("box");

                if (GUILayout.Button("Download Admob Plugin")) Application.OpenURL("https://github.com/googleads/googleads-mobile-unity/releases");
#if ACTIVE_ADMOB
                GUI.backgroundColor = Color.red;
                if (GUILayout.Button("Disable Admob")) RemoveDirective("ACTIVE_ADMOB");
#else
                GUI.backgroundColor = Color.green;
                if (GUILayout.Button("Enable Admob")) AddDirective("ACTIVE_ADMOB");
#endif
                GUI.backgroundColor = defaultColor;
                GUILayout.Space(5);
                if (GUILayout.Button("Download TapJoy Plugin")) Application.OpenURL("https://ltv.tapjoy.com/d/sdks");
#if ACTIVE_TAPJOY
                GUI.backgroundColor = Color.red;
                if (GUILayout.Button("Disable Tapjoy")) RemoveDirective("ACTIVE_TAPJOY");
#else
                GUI.backgroundColor = Color.green;
                if (GUILayout.Button("Enable Tapjoy")) AddDirective("ACTIVE_TAPJOY");
#endif
                GUI.backgroundColor = defaultColor;
                GUILayout.Space(5);
                if (GUILayout.Button("Download Audience Network Plugin")) Application.OpenURL("https://developers.facebook.com/docs/audience-network/download#unity");
#if ACTIVE_FBAN
                GUI.backgroundColor = Color.red;
                if (GUILayout.Button("Disable Audience Network")) RemoveDirective("ACTIVE_FBAN");
#else
                GUI.backgroundColor = Color.green;
                if (GUILayout.Button("Enable Audience Network")) AddDirective("ACTIVE_FBAN");
#endif
                GUI.backgroundColor = defaultColor;
                GUILayout.Space(5);
                if (GUILayout.Button("Download IronSource Plugin")) Application.OpenURL("https://developers.ironsrc.com/ironsource-mobile/unity/unity-plugin/#step-1");
#if ACTIVE_IRONSOURCE
                GUI.backgroundColor = Color.red;
                if (GUILayout.Button("Disable IronSource")) RemoveDirective("ACTIVE_IRONSOURCE");
#else
                GUI.backgroundColor = Color.green;
                if (GUILayout.Button("Enable IronSource")) AddDirective("ACTIVE_IRONSOURCE");
#endif
                GUI.backgroundColor = defaultColor;
                GUILayout.Space(5);
                var style = new GUIStyle(EditorStyles.boldLabel);
                style.alignment = TextAnchor.MiddleCenter;
#if UNITY_ADS && UNITY_MONETIZATION
                GUI.backgroundColor = Color.red;
                if (GUILayout.Button("Disable Unity Ads"))
                {
                    RemoveDirective("UNITY_ADS");
                    RemoveDirective("UNITY_MONETIZATION");
                }
#else

                EditorGUILayout.HelpBox("Unity Ads must be install from Services Window", MessageType.Info);
                GUI.backgroundColor = Color.green;
                if (GUILayout.Button("Enable Unity Ads"))
                {
                    AddDirective("UNITY_ADS");
                    AddDirective("UNITY_MONETIZATION");
                }
#endif
                GUI.backgroundColor = defaultColor;
                GUILayout.EndVertical();

            }
        }
        #endregion

#endif
    }
}